# NestedSets Manager for Yii2

Виджет для управления деревом.

Внимание!
-----
Виджет рассчитан на работу с поведениями:

[Nested Sets Behavior for Yii 2](https://github.com/creocoder/yii2-nested-sets)

Usage
-----
 
  1. Подключите к вашей модели любое из указанных выше поведений
  
  2. Подключите в контроллер дополнительные actions

```
public function actions()
{
    $modelClass = 'namespace\ModelName';

    return [
        'moveNode' => [
            'class' => 'grood\nestedsets\manager\actions\MoveNodeAction',
            'modelClass' => $modelClass,
        ],
    ];
}
```  

3. Выведите виджет в удобном месте

```
use \grood\nestedsets\manager\widgets\nestable\Nestable;

<?= Nestable::widget([
    'modelClass' => 'app\modules\core\models\Service',
    'nameAttribute' => 'title',
    'layout' => "{header}\n{items}\n{footer}",
    'options' => [
        'class' => 'box box-primary',
        'contentClass' => 'box-body'
    ],
    'headerOptions' => [
        'class' => 'box-header',
        'buttonClass' => 'btn btn-success',
        'buttonText' => Yii::t('app', 'Add Node')
    ],
    'footerOptions' => [
        'class' => 'box-footer',
        'buttonClass' => 'btn btn-success',
        'buttonText' => Yii::t('app', 'Add Node')
    ]
]) ?>
```