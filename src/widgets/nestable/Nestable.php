<?php

namespace grood\nestedsets\manager\widgets\nestable;

use grood\nestedsets\manager\interfaces\TreeInterface;
use grood\nestedsets\manager\widgets\nestable\NestableAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * Class Nestable
 * @package grood\nestedsets\manager\widgets
 */
class Nestable extends Widget
{
    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerOptions = [];

    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $footerOptions = [];

    /**
     * @var string
     */
    public $id;

    /**
     * @var array
     */
    public $modelClass;

    /**
     * @var array
     */
    public $nameAttribute = 'name';

    /**
     * Behavior key in list all behaviors on model
     * @var string
     */
    public $behaviorName = 'nestedSetsBehavior';

    /**
     * Handler for render form fields on create new node
     * @var callable
     */
    public $formFieldsCallable;

    /**
     * @var array
     */
    private $_items = [];

    /**
     * @var array
     */
    private $_options = [];

    public $layout = "{header}\n{items}\n{footer}";

    /**
     * Инициализация плагина
     */
    public function init()
    {
        parent::init();

        if (empty($this->id)) {
            $this->id = $this->getId();
        }

        if ($this->modelClass == null) {
            throw new InvalidConfigException('Param "modelClass" must be contain model name');
        }

        if (null == $this->behaviorName) {
            throw new InvalidConfigException("No 'behaviorName' supplied on action initialization.");
        }

        $controller = Yii::$app->controller;
        if ($controller) {
            $this->_options['moveUrl'] = Url::to(["moveNode"]);
            $this->_options['createUrl'] = Url::to(["create"]);
            $this->_options['updateUrl'] = Url::to(["update"]);
            $this->_options['deleteUrl'] = Url::to(["delete"]);
        }

        if ($this->formFieldsCallable == null) {
            $this->formFieldsCallable = function ($form, $model) {
                /** @var ActiveForm $form */
                echo $form->field($model, $this->nameAttribute);
            };
        }

        /** @var ActiveRecord|TreeInterface $model */
        $model = new $this->modelClass;

        /** @var ActiveRecord[]|TreeInterface[] $rootNodes */
        $rootNodes = $model::find()->roots()->all();


        foreach ($rootNodes as $root) {
            /** @var ActiveRecord $root */
            array_push($this->_items, $this->prepareTree($root));
        }
    }

    /**
     * Работаем!
     */
    public function run()
    {
        NestableAsset::register($this->getView());
        $view = $this->getView();
        $view->registerJs("
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=\"csrf-token\"]').attr('content')
                }
            });

            $('#{$this->id}').nestable().on('change', function (e)
            {
                var url = $(this).data('url');
                var list = e.length ? e : $(e.target);
                var data = list.nestable('serialize');
                $.post(url, {data : data});
            });
        ");
        $content = preg_replace_callback("/{\\w+}/", function ($matches) {
            $content = $this->renderSection($matches[0]);

            return $content === false ? $matches[0] : $content;
        }, $this->layout);

        $tag = ArrayHelper::remove($this->options, 'tag', 'div');
        echo Html::tag($tag, $content, $this->options);
    }

    /**
     * Renders a section of the specified name.
     * If the named section is not supported, false will be returned.
     * @param string $name the section name, e.g., `{summary}`, `{items}`.
     * @return string|boolean the rendering result of the section, or false if the named section is not supported.
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{header}':
                return $this->renderHeader();
            case '{items}':
                return $this->renderItems();
            case '{footer}':
                return $this->renderFooter();
            default:
                return false;
        }
    }

    private function renderHeader()
    {
        $options = $this->headerOptions;
        $content = HTML::a(
            ArrayHelper::getValue($options, 'buttonText', Yii::t('grood/nestedsets', 'Add Node')),
            $this->_options['createUrl'],
            ['class' => ArrayHelper::getValue($options, 'buttonClass', 'btn btn-success')]
        );
        return Html::tag(ArrayHelper::getValue($options, 'tag', 'div'), $content, $options);
    }

    private function renderFooter()
    {
        $options = $this->footerOptions;
        $content = HTML::a(
            ArrayHelper::getValue($options, 'buttonText', Yii::t('grood/nestedsets', 'Add Node')),
            $this->_options['createUrl'],
            ['class' => ArrayHelper::getValue($options, 'buttonClass', 'btn btn-success')]
        );
        return Html::tag(ArrayHelper::getValue($options, 'tag', 'div'), $content, $options);
    }

    private function renderItems()
    {
        $content = [];
        foreach ($this->_items as $item) {
            $content[] = implode(",", $this->printLevel($item));
        }

        $content = Html::tag('ol', implode("\n", $content), ['class' => 'dd-list']);
        return Html::tag('div', $content, ['class' => 'dd dd-nestable '.ArrayHelper::remove($this->options, 'contentClass', 'box-body'), 'id' => $this->id, 'data-url' => $this->_options['moveUrl']]);
    }


    private function printLevel($level)
    {
        $content = [];
        foreach ($level as $item) {
            $htmlOptions = ['class' => 'dd-item dd3-item'];
            $htmlOptions['data-id'] = !empty($item['id']) ? $item['id'] : '';

            $content[] = Html::tag('li', implode("\n", $this->printItem($item)), $htmlOptions);
        }
        return $content;
    }


    private function printItem($item)
    {
        $content[] = Html::tag('div', '', ['class' => 'dd-handle dd3-handle']);


        $content[] = Html::tag('div',
            implode("\n", [$item['name'], $this->renderActionButtons($item)]),
            ['class' => 'dd3-content']
        );

        if (isset($item['children']) && count($item['children'])) {
            $content[] = Html::tag('ol', implode("\n", $this->printLevel($item['children'])), ['class' => 'dd-list']);
        }

        return $content;
    }

    public function renderActionButtons($item)
    {
        $buttons[] = Html::a('<i class="fa fa-pencil"></i>', $item['update-url'], [
            'class' => 'btn btn-default btn-xs flat',
            'data-toggle' => 'tooltip',
            'data-label' => Yii::t('grood/nestedsets', 'Update'),
            'data-original-title' => Yii::t('grood/nestedsets', 'Update')
        ]);
        $buttons[] = Html::a('<i class="fa fa-times"></i>', $item['delete-url'], [
            'class' => 'btn btn-danger btn-xs btn-delete flat',
            'data-toggle' => 'tooltip',
            'data-label' => Yii::t('grood/nestedsets', 'Delete'),
            'data-original-title' => Yii::t('grood/nestedsets', 'Delete'),
            'data-confirm' => Yii::t('grood/nestedsets', 'Вы уверены, что хотите удалить этот элемент?'),
            'data-method' => 'post',
        ]);

        return Html::tag('div', implode("\n", $buttons), ['class' => 'pull-right', 'style' => 'margin-top: -3px;']);
    }

    /**
     * @param ActiveRecord|TreeInterface $node
     * @return array
     */
    private function prepareTree($node)
    {
        $result = $this->getNode($node->children(1)->all());

        $root[] = [
            'id' => $node->getPrimaryKey(),
            'name' => $node->getAttribute($this->nameAttribute),
            'children' => $result,
            'update-url' => Url::to([$this->_options['updateUrl'], 'id' => $node->getPrimaryKey()]),
            'delete-url' => Url::to([$this->_options['deleteUrl'], 'id' => $node->getPrimaryKey()]),
        ];

        return $root;
    }

    /**
     * @param ActiveRecord[]|TreeInterface[] $children
     * @return array
     */
    protected function getNode($children)
    {
        $items = [];

        /** @var ActiveRecord|TreeInterface $node */
        foreach ($children as $n => $node) {
            $items[$n]['id'] = $node->getPrimaryKey();
            $items[$n]['name'] = $node->getAttribute($this->nameAttribute);
            $items[$n]['children'] = $this->getNode($node->children(1)->all());
            $items[$n]['update-url'] = Url::to([$this->_options['updateUrl'], 'id' => $node->getPrimaryKey()]);
            $items[$n]['delete-url'] = Url::to([$this->_options['deleteUrl'], 'id' => $node->getPrimaryKey()]);
        }

        return $items;
    }
}