<?php

namespace grood\nestedsets\manager\widgets\nestable;

use yii\web\AssetBundle;

/**
 * Class NestableAsset
 * @package grood\nestedsets\manager\widgets
 */
class NestableAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@vendor/grood/yii2-nested-sets-manager/src/widgets/nestable/assets';

    /**
     * @var array
     */
    public $css = [
        'jquery.nestable.css'
    ];

    /**
     * @var array
     */
    public $js = [
        'jquery.nestable.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}