<?php

namespace grood\nestedsets\manager\actions;

use grood\nestedsets\manager\interfaces\TreeInterface;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\web\HttpException;

/**
 * Class MoveNodeAction
 * @package  grood\nestedsets\manager\actions
 */
class MoveNodeAction extends BaseAction
{
    /**
     * Move a node (model) below the parent and in between left and right
     *
     * @return array
     * @throws HttpException
     */
    public function run()
    {

        $data = Yii::$app->getRequest()->getBodyParam('data');
        foreach ($data as $node) {
            try {
                $model = $this->findModel($node['id']);
                if (!$model->isRoot()) {
                    $model->makeRoot();
                }
                if (isset($node['children'])) {
                    $this->moveNode($node['children'], $node['id']);
                }
            } catch (Exception $e) {

            }
        }

        return false;
    }

    protected function moveNode($node, $parent)
    {
        foreach ($node as $n) {
            $model = $this->findModel($n['id']);
            $parentModel = $this->findModel($parent);
            $model->appendTo($parentModel);
            if (isset($n['children'])) $this->moveNode($n['children'], $n['id']);
        }
    }
}