<?php

namespace grood\nestedsets\manager\interfaces;

/**
 * Interface TreeInterface
 * @package grood\nestedsets\manager\interfaces
 */
interface TreeInterface
{
    /**
     * @return TreeQueryInterface
     */
    public function find();

    /**
     * Gets the children of the node.
     * @param integer|null $depth the depth
     * @return \yii\db\ActiveQuery
     */
    public function children($depth = null);

    /**
     * @return $this
     */
    public function makeRoot();

    /**
     * @param \yii\db\BaseActiveRecord $node
     * @return $this
     */
    public function appendTo($node);

    /**
     * @return bool
     */
    public function isRoot();
}