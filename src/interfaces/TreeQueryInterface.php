<?php

namespace grood\nestedsets\manager\interfaces;

use yii\db\ActiveQuery;


/**
 * Interface TreeQueryInterface
 * @package grood\nestedsets\manager\interfaces
 */
interface TreeQueryInterface
{
    /**
     * @return ActiveQuery
     */
    public function roots();
}